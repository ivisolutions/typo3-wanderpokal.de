Der TYPO3camp Wanderpokal
=========================

Um immer auf den neuesten Stand zu sein was den TYPO3camp Wanderpokal angeht, haben wir ihm eine eigene Seite gewidmet. [Hier](http://www.typo3-wanderpokal.de) könnt ihr immer sehen wo er sich gerade befindet und wer die aktuellen Gewinner sind.

[www.typo3-wanderpokal.de](http://www.typo3-wanderpokal.de)

Zum Berliner TYPO3camp 2013 wurde der TYPO3camp Wanderpokal eingeführt. Am Ende eines Camps werden, zusammen mit allen Teilnehmern des Camps, 
die Sessions für den Wanderpokal nominiert. Alle nominierten Sessions werden dann nochmal kurz angesprochen, und durch die Lautstärke des jeweiligen Applauses wird der Gewinner ermittelt. Der letzte Gewinner im Jahr darf den Pokal dann behalten, und reicht ihn im nächsten Jahr an das erste Camp weiter.


Info für die Camps
==================

Bitte sorgt dafür, das der Pokal für die beste Session weiter gereicht wird. Bisher wird für jeden Gewinner ein kostenfreies Ticket vom nächsten Camp bereitgestellt.

Ihr könnt uns gerne die nächsten Gewinner per Email senden (info (at) typo3-wanderpokal (punkt) de), am liebsten stellt Ihr gleich einen Pullrequest mit den Änderungen :-)

Nachdem die neuen Gewinner fest stehen, könnt ihr uns diese gerne per Email mitteilen. Optimal wäre es natürlich wenn ihr die Änderungen an der Seite direkt selbst vornehmt. 
Dazu müsst ihr nur die js/script.js anpassen. (Standort Camp, Standort(e) Gewinner) Beim Eintragen könnt ihr auf die Hausnummer verzichten. Google weiß ja eh schon genug :D 
Für die Darstellung der "Strecke" benötigt ihr jedoch die Koordinaten. Die Adressen könnt ihr [hier](http://stevemorse.org/jcal/latlon.php) in die gewünschten Koordinaten konvertieren.