$(document).ready(function() {
	$("#map").gmap3({
		map:{},
		polyline:{
			options:{
				strokeColor: "#333",
				strokeOpacity: 1.0,
				strokeWeight: 2,
				path:[
					[52.578856, 13.306026],
          [49.9456399, 11.5713346],
          [48.99157, 12.19594],
					[48.71207, 9.21407],
          [49.0102407, 8.3836251],
          [50.1043383, 8.6364011],
          [53.5510846, 9.9936818],
          [53.5532672, 10.0847569],
          [48.11633, 11.60339],
          [48.079403, 12.0899098],
          [39.5560078, 2.6219234],
				]
			}
		},
		marker:{
    		values:[
          		{address:"Soltauer Straße, 13509 Berlin", data:"<strong>TYPO3camp Berlin 2013</strong><br />10.-12. Mai 2013", 
          			options:{
          				icon: "img/camp.png"
          			}
          		},
              {address: "Sudetenstr. 26, 93073 Neutraubling", data:"Gewinner TYPO3camp Berlin<br /><strong>Robert Zierhofer</strong>",
                options:{
                  icon: "img/winner.png"
                }
              },
              {address: "Schmatzenhöhe, 95447 Bayreuth", data:"Gewinner TYPO3camp Berlin<br /><strong>Oliver Hader</strong>",
                options:{
                  icon: "img/winner.png"
                }
              },
          		{address:"Schloß Hohenheim, 70599 Stuttgart", data:"<strong>TYPO3camp Stuttgart 2013</strong><br />7.-9. Juni 2013",
          			options:{
          				icon: "img/camp.png"
          			}
              },
              {address:"Kaiserallee 13a, 76133 Karlsruhe", data:"Gewinner TYPO3camp Stuttgart<br /><strong>Sebastian Helzle</strong>",
                options:{
                  icon: "img/winner.png"
                }
              },
              {address:"Frankenallel, 60326 Frankfurt am Main", data:"Gewinner TYPO3camp Stuttgart<br /><strong>Daniel Lienert</strong>",
                options:{
                  icon: "img/winner.png"
                }
              },
              {address:"Gertrudenstraße 3, 20095 Hamburg", data:"<strong>TYPO3camp Hamburg 2013</strong><br />2.-4. August 2013",
                options:{
                  icon: "img/camp.png"
                }
              },
              {address:"Washingtonallee, 22111 Hamburg", data:"Gewinner TYPO3camp Hamburg<br /><strong>Christian Kuhn</strong>",
                options:{
                  icon: "img/winner.png"
                }
              },
              {address:"Claudius-Keller-Straße 7, 81669 München", data:"<strong>TYPO3camp München 2013</strong><br />6.-8. September 2013",
                options:{
                  icon: "img/camp.png"
                }
              },
              {address:"Eisweiherweg. 83539 Forsting", data:"Gewinner TYPO3camp München<br /><strong>Alexander Kellner</strong>",
                options:{
                  icon: "img/winner.png"
                }
              },
              {address:"Hotel Catalonia Majorica, Palma de Mallorca", data:"<strong>TYPO3camp Mallorca 2013</strong><br />20.-22. September 2013",
                options:{
                  icon: "img/camp.png"
                }
              },
              {address:"Weiße-Lamm-Gasse 1, 93047 Regensburg", data:"<strong>TYPO3camp Regensburg 2013</strong><br />25.-27. Oktober 2013",
                options:{
                  icon: "img/camp_inactive.png"
                }
              },
              {address:"Friedrich-Ebert-Str. 18, 45127 Essen", data:"<strong>TYPO3camp RheinRuhr 2013</strong><br />9.-10. November 2013",
                options:{
                  icon: "img/camp_inactive.png"
                }
              }
    		],
    		options:{
      			draggable: false
    		},
    		events:{
      			mouseover: function(marker, event, context){
        			var map = $(this).gmap3("get"),
          			infowindow = $(this).gmap3({get:{name:"infowindow"}});
        			if (infowindow){
          				infowindow.open(map, marker);
          				infowindow.setContent(context.data);
        			} else {
          				$(this).gmap3({
            				infowindow:{
              					anchor:marker,
              						options:{
              							content: context.data
              						}
            				}
          				});
        			}
      			},
      			mouseout: function(){
        			var infowindow = $(this).gmap3({get:{name:"infowindow"}});
        			if (infowindow){
          				infowindow.close();
        			}
      			}
    		}
  		}
	}, "autofit");
});